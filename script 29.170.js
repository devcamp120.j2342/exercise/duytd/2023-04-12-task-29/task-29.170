$(document).ready(function(){
    //REGION 1

    //REGION 2
    onPageLoading();
    //REGION 3
    function onPageLoading(){
        loadDataToTable();
    }
    //REGION 4
    //hàm chuyển JSON về Obj và đổ dữ liệu vào bảng
    function loadDataToTable(){
        var vNhanVienObj = JSON.parse(gEmployeeList); //chuyển JSON về Obj
        console.log(vNhanVienObj);
        var vTabel = $('.table'); //truy xuất element tabel qua class table
        //dùng vòng lặp lặp qua các danh sách nhân viên và tạo thêm hàng trong bảng
        for(var bI = 0; bI < vNhanVienObj.employees.employee.length; bI++){
            var vNewRow = $('<tr></tr>'); //tạo một hàng mới trong bảng
            //thêm các ô dữ liêuj vào hàng
            $('<td></td>').text(vNhanVienObj.employees.employee[bI].id).appendTo(vNewRow);
            $('<td></td>').text(vNhanVienObj.employees.employee[bI].firstName).appendTo(vNewRow);
            $('<td></td>').text(vNhanVienObj.employees.employee[bI].lastName).appendTo(vNewRow);
            $('<td></td>').append($('<img>').attr('src', vNhanVienObj.employees.employee[bI].photo)).appendTo(vNewRow);
            vNewRow.appendTo(vTabel.find('tbody'));
        }
    }
});